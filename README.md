Gmail Notify Icons Plain
========================

This is a grayscale iconset for the gnome-shell-extension gmail_notify designed to integrate well with other gnome-shell icons.

![illustrative screenshot](https://github.com/bigben87/gmail_notify_icons_plain/raw/master/screenshots/combined.png)


Dependency
----------

- [gmail_notify](https://extensions.gnome.org/extension/154/gmail-notify/)


Downloading
-----------

    git clone git@github.com:bigben87/gmail_notify_icons_plain.git


Installing
----------

For the current user:

    cp gmail_notify_icons_plain/*.*g ~/.local/share/gnome-shell/extensions/gmail_notify@jablona123.pl/icons/

For all users of the system:

    sudo cp  gmail_notify_icons_plain/*.*g /usr/share/gnome-shell/extensions/gmail_notify@jablona123.pl/icons/


Tidying up
----------

Afterwards the clone is no longer necessary, so you can delete it:

    rm -rf gmail_notify_icons_plain/


Activating
----------

Bring up the programm-starter with **ALT + F2** and reload the gnome-shell by running **r**.


Contributing
------------

I do not think it would be wise to try and send pull requests, because I consider this icon set pretty much done, already.
Feel free to fork and create your own iconset based upon this, though. It's licensed GPL v2 and Adam Jablonski will surely link to yours, too.
